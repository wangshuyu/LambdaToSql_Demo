﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebDemo
{
    public partial class _01_Create : System.Web.UI.Page
    {
        LambdaToSql.SqlClient DB = new LambdaToSql.SqlClient();
        protected void Page_Load(object sender, EventArgs e)
        {
            var saveFolder = "d:\\class\\";

            //生成全部实体
            DB.DbFirst.Create(saveFolder);

            //生成指定表实体对象
            DB.DbFirst.CreateByTable(saveFolder, new List<string>() { "Table_ID", "Table_Guid" });

            Response.Write("生成表实体成功!存储地址:" + saveFolder);
        }
    }
}