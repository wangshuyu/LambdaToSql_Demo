﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using LambdaToSql;
using LambdaToSql.FrameWork;
using LambdaToSql.Extended;

namespace EntityModel
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    [DataContract]
    public class Table_ID
    {
        /// <summary>
        /// 主键
        /// </summary>
        [DataMember]
        public int? ID { get; set; }

        /// <summary>
        /// 登录名称
        /// </summary>
        [DataMember]
        public string LoginName { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DataMember]
        public string PassWord { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        [DataMember]
        public string Address { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        [DataMember]
        public string ImgUrl { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [DataMember]
        public string Gender { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [DataMember]
        public string Mobile { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [DataMember]
        public string Remark { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [DataMember]
        public Nullable<int> IsDelete { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DataMember]
        public Nullable<DateTime> CreateTime { get; set; }


    }
}