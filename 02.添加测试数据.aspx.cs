﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebDemo
{
    public partial class _02_添加测试数据 : System.Web.UI.Page
    {
        LambdaToSql.SqlClient DB = new LambdaToSql.SqlClient();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //添加int主键测试数据
            for (int i = 0; i < 300; i++)
            {
                var entity = new EntityModel.Table_ID()
                {
                    LoginName = "登录用户:" + i,
                    UserName = "用户名:" + i,
                    PassWord = "密码-" + i,
                    Gender = "男",
                    IsDelete = 0,
                    Mobile = "15804066511",
                    Remark = "备注" + i,
                    Address = "地址:" + i,
                    CreateTime = DateTime.Now
                };
                var res = DB.InsertTble(entity).ExecuteNonQuery();
                //Response.Write(res + "<br />");
            }
            Response.Write("添加测试数据成功!");


            ////添加Guid类型主键测试数据
            //for (int i = 0; i < 1; i++)
            //{
            //    var entity = new EntityModel.Table_Guid()
            //    {
            //        Guid = Guid.NewGuid(),
            //        LoginName = "登录用户:" + i,
            //        UserName = "用户名:" + i,
            //        PassWord = "密码-" + i,
            //        Gender = "男",
            //        IsDelete = 0,
            //        Mobile = "15804066511",
            //        Remark = "备注" + i,
            //        Address = "地址:" + i,
            //        //CreateTime = DateTime.Now
            //    };
            //    var res = DB.InsertTble(entity).ExecuteNonQuery();
            //    //Response.Write(res + "<br />");
            //}
            //Response.Write("添加测试数据成功!");
        }
    }
}