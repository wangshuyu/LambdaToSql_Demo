USE [master]
GO
/****** Object:  Database [LambdaToSql]    Script Date: 2018/7/11 14:10:57 ******/
CREATE DATABASE [LambdaToSql]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LambdaToSql', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\LambdaToSql.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LambdaToSql_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\LambdaToSql_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LambdaToSql] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LambdaToSql].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LambdaToSql] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LambdaToSql] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LambdaToSql] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LambdaToSql] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LambdaToSql] SET ARITHABORT OFF 
GO
ALTER DATABASE [LambdaToSql] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LambdaToSql] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LambdaToSql] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LambdaToSql] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LambdaToSql] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LambdaToSql] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LambdaToSql] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LambdaToSql] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LambdaToSql] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LambdaToSql] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LambdaToSql] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LambdaToSql] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LambdaToSql] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LambdaToSql] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LambdaToSql] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LambdaToSql] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LambdaToSql] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LambdaToSql] SET RECOVERY FULL 
GO
ALTER DATABASE [LambdaToSql] SET  MULTI_USER 
GO
ALTER DATABASE [LambdaToSql] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LambdaToSql] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LambdaToSql] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LambdaToSql] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [LambdaToSql] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'LambdaToSql', N'ON'
GO
ALTER DATABASE [LambdaToSql] SET QUERY_STORE = OFF
GO
USE [LambdaToSql]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [LambdaToSql]
GO
/****** Object:  Table [dbo].[Table_Guid]    Script Date: 2018/7/11 14:10:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Guid](
	[Guid] [uniqueidentifier] NOT NULL,
	[LoginName] [nvarchar](32) NULL,
	[UserName] [nvarchar](32) NULL,
	[PassWord] [nvarchar](64) NULL,
	[Address] [nvarchar](1024) NULL,
	[ImgUrl] [nvarchar](128) NULL,
	[Gender] [nvarchar](2) NULL,
	[Mobile] [nvarchar](16) NULL,
	[Remark] [nvarchar](1024) NULL,
	[IsDelete] [int] NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Table_Guid] PRIMARY KEY CLUSTERED 
(
	[Guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table_ID]    Script Date: 2018/7/11 14:10:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_ID](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoginName] [nvarchar](32) NULL,
	[UserName] [nvarchar](32) NULL,
	[PassWord] [nvarchar](64) NULL,
	[Address] [nvarchar](1024) NULL,
	[ImgUrl] [nvarchar](128) NULL,
	[Gender] [nvarchar](2) NULL,
	[Mobile] [nvarchar](16) NULL,
	[Remark] [nvarchar](1024) NULL,
	[IsDelete] [int] NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Table1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Table_Guid] ADD  CONSTRAINT [DF_Table_Guid_Guid]  DEFAULT (newid()) FOR [Guid]
GO
ALTER TABLE [dbo].[Table_Guid] ADD  CONSTRAINT [DF_Table_Guid_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
ALTER TABLE [dbo].[Table_ID] ADD  CONSTRAINT [DF_Table1_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'Guid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'LoginName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'PassWord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'ImgUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'IsDelete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_Guid', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主键' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'登录名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'LoginName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'用户名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'PassWord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'头像' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'ImgUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'Gender'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'手机号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'Mobile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'Remark'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'IsDelete'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Table_ID', @level2type=N'COLUMN',@level2name=N'CreateTime'
GO
USE [master]
GO
ALTER DATABASE [LambdaToSql] SET  READ_WRITE 
GO
